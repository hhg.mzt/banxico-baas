 CREATE TABLE `historico_consultas` (  
    `id_historico` int (11) AUTO_INCREMENT,  
    `serie` varchar (10) DEFAULT NULL,  
    `fecha` varchar (10) DEFAULT NULL,
    `dato` float DEFAULT 0,
    PRIMARY KEY (`id_historico`)
);