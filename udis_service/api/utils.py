
def valores_min_max_promedio(datos: list) -> tuple:
    valores = [eval(dato['dato']) for dato in datos]
    return (min(valores), max(valores), sum(valores)/len(valores))

def generar_url(base_url :str, serie: str, fecha_inicio: str, fecha_final: str) -> str:
     return f"{base_url}/{serie}/datos/{fecha_inicio}/{fecha_final}"
