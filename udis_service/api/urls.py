from django.urls import path, include

from .views import UdiListController, DolarListController, TiieListController

urlpatterns = [
    path('udis/<str:fecha_inicial>/<str:fecha_final>', UdiListController.as_view(), name='udis'),
    path('dolares/<str:fecha_inicial>/<str:fecha_final>', DolarListController.as_view(), name='dolares'),
    path('tiie/<str:fecha_inicial>/<str:fecha_final>', TiieListController.as_view(), name='tiiie'),
]