import re
from functools import wraps
from rest_framework.response import Response


def  validar_fechas_busqueda(f):
    @wraps(f)
    def wrapper(self, request):

        if not  'fecha_inicio' in request.query_params or not 'fecha_final' in request.query_params:
            response = Response(
                {
                    'status': 'Bad Request',
                    'message': 'No has enviado fecha de inicio y fin'
                },
                400
            )
            return response
        if not validar_formato_fecha(request.GET.get('fecha_inicio')) or not \
            validar_formato_fecha(request.GET.get('fecha_final')):
            response = Response(
                {
                    'status': 'Bad Request',
                    'message': 'Las fechas tienen que tener el formato yyyy-mm-dd'
                },
                400
            )
            return response
        
        
        return f(self, request)
    return wrapper

def validar_formato_fecha(fecha: str) -> bool:
    x=re.search("^\d\d\d\d-\d\d-\d\d$",fecha)
    return False if x is None else True


def  validar_fechas_url(fn):
    @wraps(fn)
    def wrapper(self, request, fecha_inicial, fecha_final):
        if not validar_formato_fecha(fecha_inicial) or not validar_formato_fecha(refecha_final):
            response = Response(
                {
                    'status': 'Bad Request',
                    'message': 'Las fechas tienen que tener el formato yyyy-mm-dd'
                },
                400
            )
            return response
        return fn(self, request, fecha_inicial, fecha_final)
    return wrapper


