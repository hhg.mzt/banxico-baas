import os
import requests
from django.shortcuts import render
from django.db.models import Q
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import HistoricoConsulta
from udis_service.constants import (
    BASE_URL,
    SERIE_VALOR_UDIS,
    SERIE_VALOR_DOLAR,
    SERIE_TIEE,
    TOKEN_BANXICO,
)
from .middleware import validar_fechas_busqueda,  validar_fechas_url
from .utils import (
    valores_min_max_promedio,
    generar_url,
)





class UdiListController(APIView):
    
    #@validar_fechas_url
    def get(self, request, fecha_inicial, fecha_final):
        headers={
            'Bmx-Token': TOKEN_BANXICO
        }
        final_url = f"{BASE_URL}/{SERIE_VALOR_UDIS}/datos/{fecha_inicial}/{fecha_final}"
        response_banxico = requests.get(final_url, headers=headers)
        print(final_url)
        
        if not response_banxico.status_code == 200:
            return Response({
                'message': 'Atención banxico a respondido con error',
                'error': response_banxico.json()['error']
            }, status=status.HTTP_400_BAD_REQUEST)
        
        response_banxico = response_banxico.json()
        if 'datos' in response_banxico['bmx']['series'][0]:
            minimo, maximo, promedio = valores_min_max_promedio(response_banxico['bmx']['series'][0]['datos'])
            response_banxico['consolidado'] = {'minimo': minimo, 'maximo': maximo, 'promedio': promedio}
        series_datos = response_banxico['bmx']['series'][0]['datos']

        for serie_dato in series_datos:
            qs_new = HistoricoConsulta.objects \
                .filter(serie=SERIE_VALOR_UDIS) \
                .filter(fecha=serie_dato['fecha']) \
                .only('id_historico')
            if qs_new.count() == 0:
               new_historico = HistoricoConsulta(serie=SERIE_VALOR_UDIS, fecha=serie_dato['fecha'], dato=serie_dato['dato'])
               new_historico.save()

        return Response(response_banxico, status=status.HTTP_200_OK)


class DolarListController(APIView):

    #@validar_fechas_busqueda
    def get(self, request, fecha_inicial, fecha_final):
        headers={
            'Bmx-Token': TOKEN_BANXICO
        }
        final_url = generar_url(BASE_URL, SERIE_VALOR_DOLAR,  fecha_inicial, fecha_final)
        response_banxico = requests.get(final_url, headers=headers)
        
        if not response_banxico.status_code == 200:
            return Response({
                'message': 'Atención banxico a respondido con error',
                'error': response_banxico.json()['error']
            }, status=status.HTTP_400_BAD_REQUEST)
        
        response_banxico = response_banxico.json()
        if 'datos' in response_banxico['bmx']['series'][0]:
            minimo, maximo, promedio = valores_min_max_promedio(response_banxico['bmx']['series'][0]['datos'])
            response_banxico['consolidado'] = {'minimo': minimo, 'maximo': maximo, 'promedio': promedio}

        for serie_dato in response_banxico['bmx']['series'][0]['datos']:
            qs_new = HistoricoConsulta.objects \
                .filter(serie=SERIE_VALOR_DOLAR) \
                .filter(fecha=serie_dato['fecha']) \
                .only('id_historico')
            if qs_new.count() == 0:
               new_historico = HistoricoConsulta(serie=SERIE_VALOR_DOLAR, fecha=serie_dato['fecha'], dato=serie_dato['dato'])
               new_historico.save()
        return Response(response_banxico, status=status.HTTP_200_OK)


class TiieListController(APIView):

    #@validar_fechas_busqueda
    def get(self, request, fecha_inicial, fecha_final):
        headers={
            'Bmx-Token': TOKEN_BANXICO
        }
        final_url = generar_url(BASE_URL, SERIE_TIEE,  fecha_inicial, fecha_final)
        response_banxico = requests.get(final_url, headers=headers)
        
        if not response_banxico.status_code == 200:
            return Response({
                'message': 'Atención banxico a respondido con error',
                'error': response_banxico.json()['error']
            }, status=status.HTTP_400_BAD_REQUEST)
        
        response_banxico = response_banxico.json()
        if 'datos' in response_banxico['bmx']['series'][0]:
            minimo, maximo, promedio = valores_min_max_promedio(response_banxico['bmx']['series'][0]['datos'])
            response_banxico['consolidado'] = {'minimo': minimo, 'maximo': maximo, 'promedio': promedio}

        for serie_dato in response_banxico['bmx']['series'][0]['datos']:
            qs_new = HistoricoConsulta.objects \
                .filter(serie=SERIE_TIEE) \
                .filter(fecha=serie_dato['fecha']) \
                .only('id_historico')
            if qs_new.count() == 0:
               new_historico = HistoricoConsulta(serie=SERIE_TIEE, fecha=serie_dato['fecha'], dato=serie_dato['dato'])
               new_historico.save()

        return Response(response_banxico, status=status.HTTP_200_OK)
    