from django.db import models

# Create your models here.


class HistoricoConsulta(models.Model):
    id_historico = models.AutoField(primary_key=True)
    serie = models.CharField(max_length=10, blank=True)
    fecha = models.CharField(max_length=10, blank=True)
    dato = models.FloatField()

    class Meta:
        db_table = 'historico_consultas'