TOKEN_BANXICO = '69fc00f7a95413005702f9190d42628d76064d9c1229da36a52dc242b3522f59'

BASE_URL = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/'

URL_SERIES_RANGE = 'https://www.banxico.org.mx/SieAPIRest/service/v1/series/SF43783/datos/2022-01-01/2022-03-28'

SERIE_VALOR_DOLAR = 'SF43718'

SERIE_VALOR_UDIS = 'SP68257'

SERIE_TIEE = 'SF331451'