from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.schemas import get_schema_view


PREFIX_API = 'api/v1/'

urlpatterns = [
    path('admin/', admin.site.urls),
    path(f'{PREFIX_API}', include('api.urls')),
    path('openapi', get_schema_view(
        title="Your Project",
        description="API for all things …",
        version="1.0.0"
    ), name='openapi-schema'),
]
