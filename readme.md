API Para consumir servicios del Banco de Mexico
NOTA: Para esta versión del proyecto se utilizó la versión 3.8 de Python.

☀️ Antes de comezar


MySQL 5.7 
Una vez descargadp el proyecto dentro de la carpeta data hay un archivo docker-compose.yaml, para crear una instancia de MYSQL, por si se desea montar el motor de BD de esta manera  una archivo crear_tabla.sql en donde viene un script para crea la tabla que usa el sistema, antes crea una base de datos llamda db o si deseas indicarle otro nombre es cambio lo debes de realizar en el archivo .env, mas adelantes lo trataremos.


Activar el puerto 3306 para conexiones entrantes



Clonar este repositorio en el directorio de proyectos:


git clone https://gitlab.com/hhg.mzt/banxico-baas.git



Crear el entorno virtual para aislar las depencias del proyecto:


virtualenv RUTA_DEL_ENTORNO_VIRTUAL/NOMBRE_ENTORNO


2.1 Para ejecutar el entorno virtual es necesario ejecutar el archivo activate.bat que se encuentra en:
en Windows:

```
RUTA_DEL_ENTORNO_VIRTUAL/Scripts/activate.bat
```


en linux:

```
RUTA_DEL_ENTORNO_VIRTUAL/bin/activate
```




Instalar las dependencias en raíz del proyecto:

pip install -r requirements.txt





Configuración
Crear archivo .env, en raiz del proyecto.

No en raiz del proyecto sino en 
la carpeta udis_service al mismo nivel de mange.py
El contenido del archivo .env y iguala las variables a tu configuración

    SERVER_HOST=
    PORT=
    USER=
    PASSWORD=
    

Despliegue a Desarrollo

Ejecutar la aplicación

python manage.py runserver


ENDPOINST DE LA APLICACION

- GET /api/v1/dolares/{fecha_inicial}/{fecha_final}

	Retorno informacion sobre el tipo de cambios de los dolares, en un rango de fecha

- GET /api/v1/tiie/{fecha_inicial}/{fecha_final}

	Retorno información informacion sobre los tiee en un rango de fechas

- GET /api/v1/udis/{fecha_inicial}/{fecha_final}
	
	Retorna información sobre los udis en un rango de fecha

Las fechas deben de ir en un formato yyyy-MM-dd, de lo contraria no obtendremos una respuesta exitosa. Un mensaje como este el backed lo cacha por parte de banxico y lo renderiza en un respuesta propia.


	{
		"message": "Atención banxico a respondido con error",
		"error": {
		"mensaje": "Formato de fecha incorrecto.",
		"detalle": "El formato de la fecha enviada es incorrecto. Debe ser yyyy-MM-dd."
	}
	}


Para mas información acerca de los endpoits una vez ejecutado el proyecto puedes acceder a: /openapi


Cliente web:
git clone https://gitlab.com/hhg.mzt/banxico-app-services.git
Entra a a carpeta creada
instala las dependencias npm install
Cambios en el código:
- Banckend
	- En archivo settings.py agregaremos la dirección donde se esta ejecutando o donde se ejecutara el frontend
	
		CORS_ALLOWED_ORIGINS = [
    		'http://localhost:4200',
		]

 Fronted
 
	- en el archivo src/app/shared/prospect.service.ts, se sustiura por la ip en donde se ejecuta o se ejecutara nuestro backend.
	- 
		  BASE_URL = 'http://localhost:8000/api/v1/'

Ejecutar ng serve
Y navegar a IP:4200/
Se enrutara a la pagina por defecto y nos mostrara un interfa para seleccionar el rango de fecha y las opciones para escucher el tipo de servicio a consultar

- UDIS
- DOLAR
- TIIE

De encontrar informacion se mostrara una lista por fecha y valor de la unidad que busqueda y tendrama una opción para graficar los resultados.